import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditItemComponent } from './components/edit-item/edit-item.component';
import { ListComponent } from './components/list/list.component';
import { NewItemComponent } from './components/new-item/new-item.component';
import { ViewItemComponent } from './components/view-item/view-item.component';

const routes: Routes = [
  { path: 'newItem', component: NewItemComponent }, 
  { path: 'list', component: ListComponent }, 
  { path: 'viewItem/:id', component: ViewItemComponent },
  { path: 'updateItem/:id', component: EditItemComponent }
];
//{ path: 'newItem', loadChildren: () => import('./components/new-item/new-item.module').then(m => m.NewItemModule) }

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
