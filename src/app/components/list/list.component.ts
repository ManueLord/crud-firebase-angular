import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CongurationService } from 'src/app/service/conguration.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  person: any;

  constructor(private config: CongurationService, private router: Router) {
    this.config.returnPerson().subscribe(people => {
      this.person = people;
    });
  }

  ngOnInit(): void {
  }

  onGoToEdit(id: string):void {
    this.router.navigate(["updateItem", id])
  }
  onGoToSee(id: string):void {
    this.router.navigate(["viewItem", id])
  }
  onGoToDelete(id:string):void {
    this.config.deleteItem(id);
  }
}
