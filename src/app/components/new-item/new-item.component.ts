import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { CongurationService } from 'src/app/service/conguration.service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.scss']
})
export class NewItemComponent implements OnInit {

  dataPerson = {name:'', year: 0, date: '', status:'', gender:''} 

  constructor(private router: Router, private config: CongurationService, private location: Location) { }

  ngOnInit(): void {
  }
  
  back(): void {
    this.location.back()
  }

  addPerson(){
    this.config.addItem(this.dataPerson);
    this.router.navigate(['list']);
  }
}
