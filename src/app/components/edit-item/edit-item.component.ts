import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { FormBuilder, FormGroup } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { CongurationService } from 'src/app/service/conguration.service'

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss']
})
export class EditItemComponent implements OnInit {
  userRef: any
  public editUser!: FormGroup;
  
  constructor(private config: CongurationService, private act: ActivatedRoute,  private router: Router, public fb: FormBuilder, private location: Location) {}

  ngOnInit(): void {
    const id = this.act.snapshot.paramMap.get('id')
      this.config.viewPerson(id).subscribe(people => {
        this.userRef = people
        this.editUser = this.fb.group({
          name: [this.userRef.name],
          gender: [this.userRef.gender],
          date: [this.userRef.date],
          status: [this.userRef.status],
          year: [this.userRef.year]
        })
      })
  }

  back(): void {
    this.location.back()
  }

  updatePerson(){
    const id = this.act.snapshot.paramMap.get('id');
    this.config.updateItem(this.editUser.value, id);
    this.router.navigate(['list'])
  }
}
