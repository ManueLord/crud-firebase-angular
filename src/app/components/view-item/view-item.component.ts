import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CongurationService } from 'src/app/service/conguration.service';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.scss']
})
export class ViewItemComponent implements OnInit {
  userRef: any

  constructor(private config: CongurationService, private act: ActivatedRoute,  private router: Router) {
    const id = this.act.snapshot.paramMap.get('id');
      this.config.viewPerson(id).subscribe(people => {
        this.userRef = people;
      });
   }

   ngOnInit(): void {
  }

  updateItem(): void {
    const id = this.act.snapshot.paramMap.get('id');
    this.router.navigate(["updateItem", id]);
  }

  deleteItem(): void {
    const id = this.act.snapshot.paramMap.get('id');
    this.config.deleteItem(id);
  }

  returnList() {
    this.router.navigate(["list"]);
  }
}
