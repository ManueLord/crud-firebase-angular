export interface Struct {
    name: string;
    year: number;
    date: string;
    status: string;
    gender: string;
}
