import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Struct } from '../interface/struct';

@Injectable({
  providedIn: 'root'
})
export class CongurationService {

  private itemsCollection: AngularFirestoreCollection<Struct>;
  //private itemDoc: AngularFirestoreDocument<Struct>;

  person: Observable<Struct[]>;

  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Struct>('person');
    //this.person = this.itemsCollection.valueChanges();
    this.person = this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Struct;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  viewPerson(id: any){
    return this.afs.collection('person').doc(id).valueChanges();
  }

  returnPerson(){
    return this.person;
  }

  addItem(item: Struct) {
    this.itemsCollection.add(item);
  }

  updateItem(item: Struct, id: any){
    this.afs.doc('person/' + id).update(item);
  }

  deleteItem(id: any){
    this.afs.doc<Struct>("person/"+id).delete();
    //this.itemDoc.delete();
  }
}
