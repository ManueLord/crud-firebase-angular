// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDAQzrOSUbq78VZx34SnC7deYT-u6yiKx0",
    authDomain: "crud-angular-db.firebaseapp.com",
    projectId: "crud-angular-db",
    storageBucket: "crud-angular-db.appspot.com",
    messagingSenderId: "145509213647",
    appId: "1:145509213647:web:ce5213fd72b6e4ce1e1eba"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
